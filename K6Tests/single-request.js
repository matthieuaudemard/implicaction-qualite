import {check, sleep} from 'k6'
import http from 'k6/http'

const SITE_URI = __ENV.SITE_URI;

// See https://k6.io/docs/using-k6/options
export const options = {
    stages: [
        {duration: '1m', target: 5}, // simulate ramp-up of traffic from 1 to 5 users over 1 minute.
        {duration: '3m', target: 3}, // stay at 3 users for 3 minutes
        {duration: '1m', target: 1}, // ramp-down to 1 user
    ],
    thresholds: {
        http_req_failed: ['rate<0.02'], // http errors should be less than 2%
        http_req_duration: ['p(95)<5000'], // 95% requests should be below 2s
    },
    vus: 5,
    ext: {
        loadimpact: {
            distribution: {
                'amazon:us:ashburn': {loadZone: 'amazon:us:ashburn', percent: 100},
            },
        },
    },
}

export default function () {
    let response = http.get(SITE_URI)
    sleep(1)
    check(response, {
        "status is 200": (r) => r.status === 200
    });
}
