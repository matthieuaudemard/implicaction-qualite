import http from 'k6/http';
import {check, group} from 'k6';

const API_URI = __ENV.API_URI;
const USERNAME = __ENV.USERNAME;
const USERPASS = __ENV.USERPASS;
const CONTENT_TYPE_JSON = 'application/json';
const POST_BASE_URI = `${API_URI}/posts`;

export default function () {
    const params = {
        headers: {
            'Content-Type': CONTENT_TYPE_JSON,
        },
        tags: {
            name: 'get-user_token', // first request
        },
    };
    group('access to contents', (_) => {
        // posts request without login
        params.tags.name = 'get-latest-posts';
        const posts_response_success = http.get(`${POST_BASE_URI}/latest/5`, params);
        check(posts_response_success, {
            'is listing status 200': (r) => r.status === 200,
            'has id': (r) => r.json()[0].id
        });

        const postPayloadStringified = JSON.stringify({
            "groupId": 33,
            "name": "string",
            "url": "string",
            "description": "string",
        });

        // post creation request without authentication
        params.tags.name = 'create-content';
        let posts_create_response = http.post(POST_BASE_URI, postPayloadStringified, params);
        check(posts_create_response, {
            'is status 403': (r) => r.status === 403,
        })

        // login request
        const login_body = JSON.stringify({
            username: USERNAME,
            password: USERPASS,
        });
        const login_response = http.post(`${API_URI}/auth/login`, login_body, params);
        check(login_response, {
            'is login status 200': (r) => r.status === 200,
            'is auth token present': (r) => r.json().hasOwnProperty('authenticationToken'),
        });
        params.headers['Authorization'] = `Bearer ${login_response.json()['authenticationToken']}`;
        params.headers['Accept'] = CONTENT_TYPE_JSON;
        params.headers['X-XSRF-TOKEN'] = http.cookieJar().cookiesForURL(login_response.url)['XSRF-TOKEN'];

        // post creation with authentication
        posts_create_response = http.post(POST_BASE_URI, postPayloadStringified, params);
        console.log(posts_create_response.status);
        check(posts_create_response, {
            'is status created': (r) => r.status === 201,
        })
    });
}
