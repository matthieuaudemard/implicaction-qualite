import http from 'k6/http';
import {check, group, sleep} from 'k6';

const options = {
    vus: 1000,
    duration: '600s',
};

const SLEEP_DURATION = 0.1;
const USERNAME = __ENV.USERNAME;
const USERPASS = __ENV.USERPASS;
const API_URI = __ENV.API_URI;
const CONTENT_TYPE_JSON = 'application/json';

export default function () {
    const body = JSON.stringify({
        username: USERNAME,
        password: USERPASS,
    });
    const params = {
        headers: {
            'Content-Type': CONTENT_TYPE_JSON,
        },
        tags: {
            name: 'login', // first request
        },
    };

    group('simple user journey', (_) => {
        // Login request
        const login_response = http.post(`${API_URI}/auth/login`, body, params);
        check(login_response, {
            'is status 200': (r) => r.status === 200,
            'is auth token present': (r) => r.json().hasOwnProperty('authenticationToken'),
        });

        const currentUser = login_response.json()['currentUser'];
        const refreshToken = login_response.json()['refreshToken'];

        params.headers['Authorization'] = `Bearer ${login_response.json()['authenticationToken']}`;
        params.headers['Accept'] = CONTENT_TYPE_JSON;
        params.headers['X-XSRF-TOKEN'] = http.cookieJar().cookiesForURL(login_response.url)['XSRF-TOKEN'];

        // Get user profile request
        params.tags.name = 'get-user-profile';
        const user_profile_response = http.get(
            `${API_URI}/users/${currentUser.id}`,
            params
        );
        sleep(SLEEP_DURATION);

        // Update user profile request
        currentUser.firstname = 'user_' + __ITER;
        params.tags.name = 'update-user-profile';
        const update_profile_response = http.put(
            `${API_URI}/users/`,
            JSON.stringify(currentUser),
            params
        );
        sleep(SLEEP_DURATION);

        // Logout request
        params.tags.name = 'logout';
        const logout_response = http.post(`${API_URI}/auth/logout`, JSON.stringify({refreshToken}), params);
        sleep(SLEEP_DURATION);
    });
}
