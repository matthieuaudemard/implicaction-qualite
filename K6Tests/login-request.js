import http from 'k6/http';
import {check, group, sleep} from 'k6';

const SLEEP_DURATION = 0.1;
const USERNAME = __ENV.USERNAME;
const USERPASS = __ENV.USERPASS;
const API_URI = __ENV.API_URI;
const CONTENT_TYPE_JSON = 'application/json';

export default function () {
    const urlLogin = `${API_URI}/auth/login`;
    const urlCommunity = `${API_URI}/users/community`;

    const params = {
        headers: {
            'Content-Type': CONTENT_TYPE_JSON,
        },
        tags: {
            name: 'login',
        },
    };

    const payload_success = JSON.stringify({
        username: USERNAME,
        password: USERPASS,
    });

    group('login / logout operations', (_) => {
        const login_response_success = http.post(urlLogin, payload_success, params);
        check(login_response_success, {
            'is login status 200': (r) => r.status === 200,
            'has refreshToken': (r) => r.json().hasOwnProperty('refreshToken'),
            'has bearer token': (r) => r.json().hasOwnProperty('authenticationToken'),
        });

        const refreshToken = login_response_success.json()['refreshToken'];

        params.headers['Authorization'] = `Bearer ${login_response_success.json()['authenticationToken']}`;
        params.headers['Accept'] = CONTENT_TYPE_JSON;
        params.headers['X-XSRF-TOKEN'] = http.cookieJar().cookiesForURL(login_response_success.url)['XSRF-TOKEN'];

        const community_response_success = http.get(urlCommunity, params);
        check(community_response_success, {
            'is community status 200': (r) => r.status === 200,
        });

        // Logout request
        params.tags.name = 'logout';
        const logout_response = http.post(`${API_URI}/auth/logout`, JSON.stringify({refreshToken}), params);
        check(logout_response, {
            'is logout status 200': (r) => r.status === 200,
            'is content body correct': (r) => r.body === 'Refresh Token Deleted Successfully'
        })
        sleep(SLEEP_DURATION);

        const payload_failure = JSON.stringify({
            username: USERPASS + 'x', // modification du nom d'utilisateur pour rendre la procédure d'identification invalide
            password: USERPASS,
        });

        const login_response_failure = http.post(urlLogin, payload_failure, params);
        check(login_response_failure, {
            'is login status 401': (r) => r.status === 401,
        });
    });
}
